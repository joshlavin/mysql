﻿-- MySQL dump 10.13  Distrib 5.5.35, for Linux (i686)

--
-- Table structure for table `qcomments`
--

DROP TABLE IF EXISTS `qcomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qcomments` (
  `code_seq` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(32) DEFAULT NULL,
  `comment` text,
  `date_time` varchar(32) DEFAULT NULL,
  `user_name` varchar(64) DEFAULT NULL,
  `processed` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`code_seq`),
  KEY `qcomments_version` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=13899 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qfields`
--

DROP TABLE IF EXISTS `qfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qfields` (
  `version` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`version`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qimages`
--

DROP TABLE IF EXISTS `qimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qimages` (
  `version` varchar(32) NOT NULL,
  `image_field` varchar(32) NOT NULL,
  `contents` mediumblob,
  PRIMARY KEY (`version`,`image_field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qref`
--

DROP TABLE IF EXISTS `qref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qref` (
  `code_seq` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(32) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `ref` text,
  `ref_tag` int(11) DEFAULT NULL,
  `page` char(12) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_contents` blob,
  `mod_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`code_seq`),
  KEY `moodle_id` (`version`)
) ENGINE=InnoDB AUTO_INCREMENT=3801 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qref_tags`
--

DROP TABLE IF EXISTS `qref_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qref_tags` (
  `code_seq` int(11) NOT NULL AUTO_INCREMENT,
  `coderef` varchar(128) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `doc_num` char(12) DEFAULT NULL,
  PRIMARY KEY (`code_seq`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=468 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `version` varchar(32) NOT NULL DEFAULT '',
  `assigned_to` varchar(32) DEFAULT NULL,
  `status` varchar(128) NOT NULL DEFAULT 'NEW',
  `test_name` varchar(64) DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `git_object` varchar(255) DEFAULT NULL,
  `question` text,
  `choice_a` text,
  `choice_b` text,
  `choice_c` text,
  `choice_d` text,
  `choice_e` text,
  `choice_f` text,
  `answer` varchar(255) DEFAULT NULL,
  `category` varchar(64) DEFAULT NULL,
  `retain_order` varchar(255) DEFAULT NULL,
  `image_a` varchar(255) DEFAULT NULL,
  `image_b` varchar(255) DEFAULT NULL,
  `image_c` varchar(255) DEFAULT NULL,
  `image_d` varchar(255) DEFAULT NULL,
  `image_e` varchar(255) DEFAULT NULL,
  `image_f` varchar(255) DEFAULT NULL,
  `image_top` varchar(255) DEFAULT NULL,
  `image_bottom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

